# Palopoli - Marchetti 2020

This repository has moved to https://gitlab.com/sbgunq/publications/palopoli-marchetti-2020-rates (Contained data and code for Palopoli, Marchetti et al, 2020)
